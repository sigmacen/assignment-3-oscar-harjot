The purpose of the game is to escape from an alien planet located in the Andromeda Galaxy. An asteroid hit the planet and the ship has to escape immediately.

The ship has 3 lives and has to go through 9 different levels. The difficulty is increasing with each level.
--------------------------------------------------------------------------------------------
Core of the Application (2 Weeks)
Collision Detection
Obstacle Detection
Movement (Accelerometer)
Design
![Main menu.png](https://bitbucket.org/repo/kKBxbg/images/2472266659-Main%20menu.png)
(https://bitbucket.org/repo/kKBxbg/images/1930929078-level2.png)![level1.jpeg](https://bitbucket.org/repo/kKBxbg/images/2329159148-level1.jpeg)![level3.png](https://bitbucket.org/repo/kKBxbg/images/2049635149-level3.png)