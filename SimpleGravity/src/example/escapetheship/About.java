package example.escapetheship;



import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class About extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main3);
		TextView t = (TextView) findViewById(R.id.about);
		t.setText("This is an application for assignment 3\nExtension to classic Copter Game\nUse of accelerometer to control the ship.");
	}
	
}


