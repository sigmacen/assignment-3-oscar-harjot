package example.escapetheship;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class SimpleGravity extends Activity {
    private GameLoop gameLoop;
    Button buttonStart;
    Button buttonBack;
	MediaPlayer defiance;
    

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    //  requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.main2); // set the content view or our widget lookups will fail
        
		defiance = MediaPlayer.create(this, R.raw.defiance);
		
		defiance.setLooping(true);
		 defiance.start();
     
     gameLoop = (GameLoop)findViewById(R.id.gameLoop);
        
      final Button btnRestart = (Button)findViewById(R.id.btnRestart);
        btnRestart.setOnClickListener(new OnClickListener()
        {	
			@Override
			public void onClick(View v)
			{
				gameLoop.restart();
				gameLoop.invalidate();
			}
		});
        
        final Button btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new OnClickListener()
        {	
			@Override
			public void onClick(View v)
			{
				gameLoop.close();
				defiance.stop();
				finish();
			
			}
		});  
   }
}