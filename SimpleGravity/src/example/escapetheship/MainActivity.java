package example.escapetheship;


import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity implements View.OnClickListener {
	TextView buttonStart,buttonExit,buttonAbout,buttonHelp;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		buttonStart = (TextView) findViewById(R.id.start); 
		buttonStart.setOnClickListener(this);
        buttonAbout= (TextView) findViewById(R.id.about); 
        buttonAbout.setOnClickListener(this);
        buttonHelp= (TextView) findViewById(R.id.help); 
        buttonHelp.setOnClickListener(this);
        
        buttonExit = (TextView) findViewById(R.id.exit); 
		buttonExit.setOnClickListener(this);

//	addListenerOnButton();
	}
 private void startClick(){
	 Intent i  = new Intent(this, SimpleGravity.class);
	 
	 startActivity(i);
	}

	@Override
			public void onClick(View v) {

				// TODO Auto-generated method stub
				if(v.getId()==R.id.start){
				startClick();
				}
				else if(v.getId()==R.id.exit){
					// exit application
					AlertDialog.Builder exitDialog = new AlertDialog.Builder(this);
					exitDialog
							.setMessage("Are you sure You want to exit")
							.setCancelable(false)
							.setPositiveButton("YES",
									new DialogInterface.OnClickListener() {
										// On
										// clicking
										// "Yes"
										// button

										public void onClick(DialogInterface dialog,
												int id) {
											System.out.println(" onClick ");
											closeApplication(); // Close Application
																// method called
										}
									})
							.setNegativeButton("NO",
									new DialogInterface.OnClickListener() {
										// On
										// clicking
										// "No"
										// button
										public void onClick(DialogInterface dialog,
												int id) {
											dialog.cancel();
										}
									});

					AlertDialog alert = exitDialog.create();
					alert.show();

				}
				else if(v.getId()==R.id.about){
					Intent i  = new Intent(this, About.class);
					 startActivity(i);
				}
				else if(v.getId()==R.id.help){
					Intent i  = new Intent(this, Help.class);
					 startActivity(i);
				}

	}

@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	private void closeApplication() {
		System.out.println("closeApplication ");
		this.finish();
	}

}
