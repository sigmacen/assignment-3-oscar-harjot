package example.escapetheship;

import java.util.Random;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.Point;
import android.graphics.Shader;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;

public class GameLoop extends SurfaceView implements Runnable,
SurfaceHolder.Callback, OnTouchListener,SensorEventListener {

static final int REFRESH_RATE = 20;
static final double GRAVITY = 0.01;

double speedFactor;
int currentLifes;
int currentLevel;
int obstacleNumber;
int stageCompleted;

Paint paintMars;
Paint p;

Thread main;

Paint paint = new Paint();


Point startL = new Point();
Point startR = new Point();
//Test
Bitmap background;
Bitmap bitmapBack;
Bitmap bitmapRocket;
Bitmap bitmapSide;
Bitmap bitmapCenter;
Bitmap bitmapExplode;
Bitmap bitmapAsteroid;
Bitmap bitmapGameover;
Bitmap bitmapEnd;

boolean thruster = false;
boolean thrusterR = false;
boolean mainThruster = false;

private SensorManager mSensorManager; 
private Sensor mAccelerometer; 

Canvas canvas;
SurfaceHolder holder;

MediaPlayer center;
MediaPlayer side;
MediaPlayer explotion;

PathMeasure pm;

boolean first = false;
boolean firstObs = false;
boolean firstTime = false;

int xcorL[];
int ycorL[];

int xcorR[];
int ycorR[];

int obsX[];
int obsY[];


int currentPosition=0;

int w,h;

Canvas offscreen;
Bitmap buffer;

boolean downPressed = false;
boolean leftPressed = false;
boolean rightPressed = false;
Boolean gameover = false;
Boolean back = false;

Shader sMars;

float x, y;
int width = 0;

double t = 1.5;

Path path,path1;

public GameLoop(Context context) {
super(context);

init(context);
}

public GameLoop(Context context, AttributeSet attrs, int defStyle) {
super(context, attrs, defStyle);

init(context);
}

public GameLoop(Context context, AttributeSet attrs) {
super(context, attrs);

init(context);
}

public void  initImages()
{
	if(currentLevel==1)
	{
		bitmapBack = BitmapFactory.decodeResource(getResources(), R.drawable.magmabck);
		sMars = new BitmapShader(BitmapFactory.decodeResource(getResources(), R.drawable.magma), Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
		paintMars.setShader(sMars);
	}
	else
	{
		if(currentLevel==2)
		{
			bitmapBack = BitmapFactory.decodeResource(getResources(), R.drawable.amatistabck);
			sMars = new BitmapShader(BitmapFactory.decodeResource(getResources(), R.drawable.amatista), Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
			paintMars.setShader(sMars);
		}
		else
		{
			if(currentLevel==3)
			{
				bitmapBack = BitmapFactory.decodeResource(getResources(), R.drawable.stonebck);
				sMars = new BitmapShader(BitmapFactory.decodeResource(getResources(), R.drawable.stone), Shader.TileMode.REPEAT, Shader.TileMode.REPEAT);
				paintMars.setShader(sMars);
			}
		}
	}

}

public void init(Context context)
{
//Setup the bitmaps
bitmapRocket = BitmapFactory.decodeResource(getResources(), R.drawable.rocket);
bitmapSide = BitmapFactory.decodeResource(getResources(), R.drawable.thruster);
bitmapCenter = BitmapFactory.decodeResource(getResources(), R.drawable.main_flame);
bitmapExplode = BitmapFactory.decodeResource(getResources(), R.drawable.explode);
bitmapAsteroid = BitmapFactory.decodeResource(getResources(), R.drawable.asteroid);
bitmapGameover = BitmapFactory.decodeResource(getResources(), R.drawable.gameover);
bitmapEnd = BitmapFactory.decodeResource(getResources(), R.drawable.end);

//setup the Sounds
center = MediaPlayer.create(context, R.raw.center);
side = MediaPlayer.create(context, R.raw.side2);
explotion = MediaPlayer.create(context, R.raw.explosion);

y=0;

//Paints
paintMars = new Paint();
p = new Paint();
					
setOnTouchListener(this);

getHolder().addCallback(this);

//Accelerometer
mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

currentLifes = 3;
currentLevel = 1;
stageCompleted = 1200;

//Setup image design
initImages();

}


@Override
protected void onSizeChanged(int w1, int h1, int oldw, int oldh) {
super.onSizeChanged(w1, h1, oldw, oldh);

	width = w1;
	
	if(!first)
	{
		first = true;
		x = width /2;
		y=0;
	}
}

public void ObstacleGenerator(int leftX[], int rightX[], int bothY[],int numObstacles)
{
	int pointsLenght = leftX.length;
	Random r = new Random();
	Random r1 = new Random();
	
	int ranPoint=0;
	int ranX=0;
	
	obsX = new int[numObstacles];
	obsY = new int[numObstacles];
	
	
	for(int i=0;i<numObstacles;i++)
	{
		ranPoint=r.nextInt(pointsLenght-4)+4;
		
		ranX = r1.nextInt((rightX[ranPoint]-300)-(leftX[ranPoint])+100) + (leftX[ranPoint]+100);
		
		obsX[i] = ranX;
		obsY[i] = bothY[ranPoint];
	}	
}


// terrain
public void Terrain(int distance)
{
	path = new Path();
	path1 = new Path();
	
	startL.x = 0;
	startL.y = h;
	
	startR.x = w;
	startR.y = h;
	
	Point beforeEndL = new Point();
	beforeEndL.x = 0;
	beforeEndL.y = 0;
	
	Point beforeEndR = new Point();
	beforeEndR.x = w;
	beforeEndR.y = 0;
	
	if(currentLevel == 1)
	{
	
	int xcor2[]={startR.x,w-200,w-400,w-150,w-200,w-150,w-180,w-200,w-150,w-180,w-200,w-150,w-160,w-200,w-100,w-80,w-200,w-150,w-400,w-300,w-250,w-300,w-150,w-500,w-150,w-300,w-10,w-400,w-150};
	int ycor2[]={startR.y,650-distance,500-distance,450-distance,400-distance,350-distance,310-distance,280-distance,250-distance,200-distance,150-distance,100-distance,50-distance,20-distance,-20-distance,-50-distance,-100-distance,-200-distance,-250-distance,-300-distance,-350-distance,-400-distance,-450-distance, -500-distance,-550-distance, -600-distance, -800-distance,-810-distance,-900-distance};
	
	
	int xcor1[]={startL.x,200,400,150,200,150,180,200,150,180,200,150,160,200,100,80,200,150,400,300,250,300,150,500,150,300,10,400,150};
	int ycor1[]={startL.y,650-distance,500-distance,450-distance,400-distance,350-distance,310-distance,280-distance,250-distance,200-distance,150-distance,100-distance,50-distance,20-distance,-20-distance,-50-distance,-100-distance,-200-distance,-250-distance,-300-distance,-350-distance,-400-distance,-450-distance, -500-distance,-550-distance, -600-distance, -800-distance,-810-distance,-900-distance};
	
	xcorL = xcor1;
	ycorL = ycor1;
	
	xcorR = xcor2;
	ycorR = ycor2;
	
	
	speedFactor = 5;
	obstacleNumber = 10;
	stageCompleted = 1200;
	
	}
	else
	{
		if(currentLevel == 2)
		{
			int xcor2[]={startR.x,w-200,w-400,w-150,w-200,w-150,w-180,w-200,w-150,w-180,w-200,w-150,w-160,w-200,w-100,w-80,w-200,w-150,w-400,w-300,w-250,w-300,w-150,w-500,w-150,w-300,w-10,w-400,w-150};
			int ycor2[]={startR.y,650-distance,300-distance,200-distance,10-distance,-100-distance,-300-distance,-400-distance,-450-distance,-600-distance,-800-distance,-1000-distance,-1200-distance,-1300-distance,-1500-distance,-1600-distance,-1900-distance,-2100-distance,-2300-distance,-2600-distance,-2800-distance,-3000-distance,-3300-distance, -3500-distance,-3600-distance, -4000-distance, -4200-distance,-4700-distance,-5000-distance};
	
	
			int xcor1[]={startL.x,200,400,150,200,150,180,200,150,180,200,150,160,200,100,80,200,150,400,300,250,300,150,500,150,300,10,400,150};
			int ycor1[]={startL.y,650-distance,300-distance,200-distance,10-distance,-100-distance,-300-distance,-400-distance,-450-distance,-600-distance,-800-distance,-1000-distance,-1200-distance,-1300-distance,-1500-distance,-1600-distance,-1900-distance,-2100-distance,-2300-distance,-2600-distance,-2800-distance,-3000-distance,-3300-distance, -3500-distance,-3600-distance, -4000-distance, -4200-distance,-4700-distance,-5000-distance};
	
			xcorL = xcor1;
			ycorL = ycor1;
	
			xcorR = xcor2;
			ycorR = ycor2;
			
			speedFactor = 10;
			obstacleNumber = 50;
			stageCompleted = 5000;
		}
		else
		{
			if(currentLevel == 3)
			{
				int xcor2[]={startR.x,w-200,w-400,w-150,w-200,w-150,w-180,w-200,w-150,w-180,w-200,w-150,w-160,w-200,w-100,w-80,w-200,w-150,w-400,w-300,w-250,w-300,w-150,w-500,w-150,w-300,w-10,w-400,w-150};
				int ycor2[]={startR.y,650-distance,100-distance,-500-distance,-1000-distance,-1200-distance,-1300-distance,-1700-distance,-2000-distance,-2600-distance,-2800-distance,-3000-distance,-3200-distance,-3600-distance,-4500-distance,-5600-distance,-5900-distance,-6100-distance,-7300-distance,-7600-distance,-7800-distance,-9000-distance,-9300-distance, -9500-distance,-9600-distance, -10000-distance, -10200-distance,-10700-distance,-13000-distance};
	
	
				int xcor1[]={startL.x,200,400,150,200,150,180,200,150,180,200,150,160,200,100,80,200,150,400,300,250,300,150,500,150,300,10,400,150};
				int ycor1[]={startL.y,650-distance,100-distance,-500-distance,-1000-distance,-1200-distance,-1300-distance,-1700-distance,-2000-distance,-2600-distance,-2800-distance,-3000-distance,-3200-distance,-3600-distance,-4500-distance,-5600-distance,-5900-distance,-6100-distance,-7300-distance,-7600-distance,-7800-distance,-9000-distance,-9300-distance, -9500-distance,-9600-distance, -10000-distance, -10200-distance,-10700-distance,-13000-distance};
	
				xcorL = xcor1;
				ycorL = ycor1;
	
				xcorR = xcor2;
				ycorR = ycor2;
				
				speedFactor = 15;
				obstacleNumber = 100;
				stageCompleted = 12000;
			}
		}
	}
	
	
	for (int i = 0; i < xcorL.length; i++) {
			path.lineTo(xcorL[i], ycorL[i]);
		
	}
	
	path.lineTo(beforeEndL.x,beforeEndL.y);
	path.lineTo(startL.x,startL.y);
	
	for (int i = 0; i < xcorR.length; i++) {
		path1.lineTo(xcorR[i], ycorR[i]);
	
	}
	
	path1.lineTo(beforeEndR.x,beforeEndR.y);
	path1.lineTo(startR.x,startR.y);
	
	if(!firstObs)
	{
		ObstacleGenerator(xcorL,xcorR,ycorR,obstacleNumber);
		firstObs = true;
	}

}

public void run() {
while(true)
{
	while (!gameover && !back)
	{
		
		if(x>=w)
			x=1;
		if(x<=0)
			x=w-1;
		
	
		canvas = null;
		holder = getHolder();
		synchronized (holder) {
			canvas = holder.lockCanvas();
			
			if(!firstTime)
			{
				restart();
				firstTime = true;
			}
			
			t = t - speedFactor;
			
			Terrain((int)t);
			
					
		
			canvas.drawBitmap(bitmapBack, 0, 0, null);
			
			
			if(y==0)
				y=h;
			
			canvas.drawBitmap(bitmapRocket, x, (float)y, null);
			
			if(thruster)
			{
				canvas.drawBitmap(bitmapSide, x, y+40, null);
				thruster = false;
			}
			
			if(thrusterR)
			{
				canvas.drawBitmap(bitmapSide, x+40, y+40, null);
				thrusterR = false;
			}
			
			if(mainThruster)
			{
				canvas.drawBitmap(bitmapCenter, x+15, y+60, null);
				mainThruster = false;
			}
			
			y = h/2;
			
			
			canvas.drawPath(path, paint);
			canvas.drawPath(path,paintMars);
			
			canvas.drawPath(path1, paint);
			canvas.drawPath(path1,paintMars);
			
		}
		
		paint.setColor(Color.WHITE);
		paint.setTextSize(30);
		
		canvas.drawText("Lifes: "+currentLifes+"/3", 100,30, paint);
		
		canvas.drawText("Level: "+currentLevel, 300,30, paint);
		
		canvas.drawText("Distance: "+(int)t*-1+"/"+stageCompleted, 500,30, paint);
		
		
		for(int i=0;i<obsX.length;i++)
		{
			float yTemp = (float) ((float)obsY[i]-t);
			canvas.drawBitmap(bitmapAsteroid, obsX[i],yTemp , null);
		}
		
		if (contains(xcorL, ycorL, x, y + 46) || contains(xcorR, ycorR, x+46, y + 46) || ObstacleDetection(x, y + 46)) {
						
			p.setColor(Color.WHITE);
			p.setTextSize(20);
			

			canvas.drawBitmap(bitmapExplode, x, y, null);
			side.stop();
			center.stop();
			explotion.start();

			currentLifes--;
			
			if(currentLifes>0)
			{
				reset();
			}
			else
			{
				gameover = true;
				canvas.drawBitmap(bitmapGameover, 70, 20, null);
			}
		}
		else
		{
			if((int)(t*-1)==stageCompleted)
			{
				
		
     			p.setColor(Color.WHITE);
				p.setTextSize(50);
				
				canvas.drawText("Stage completed!", w/2,h/2, p);
				canvas.drawText("Loading...", w/2,h/2, p);			
				
				
				if(currentLifes>0)
				{
					if(currentLevel == 3)
					{
						gameover = true;
						canvas.drawBitmap(bitmapEnd, 70, 20, null);
					}
					else
					{
						currentLevel++;
						initImages();
						reset();
					}
				}
				else
				{
					gameover = true;
					canvas.drawBitmap(bitmapGameover, 70, 20, null);
				}
			}
		}

			try {
				Thread.sleep(REFRESH_RATE);
			} catch (Exception e) {
			}

		holder.unlockCanvasAndPost(canvas);
	}
}

}

public boolean ObstacleDetection(double x0, double y0)
{
	for(int i=0;i<obsX.length;i++)
	{
		int size = 40;
		
		int[] tempObsX ={obsX[i]+size,obsX[i]-size,obsX[i]-size,obsX[i]+size};
		int[] tempObsY ={(int) (obsY[i]-size-t+40),(int) (obsY[i]-size-t+40),(int) (obsY[i]+size-t+40),(int) (obsY[i]+size-t+40)};
		
		boolean collision = contains(tempObsX,tempObsY,x0,y0);
		
		if(collision)
			return true;
	}
	
	return false;
}

public boolean contains(int[] xcor, int[] ycor, double x0, double y0) {
int crossings = 0;

for (int i = 0; i < xcor.length - 1; i++) {
	int x1 = xcor[i];
	int x2 = xcor[i + 1];

	int y1 = ycor[i];
	int y2 = ycor[i + 1];

	int dy = y2 - y1;
	int dx = x2 - x1;

	double slope = 0;
	if (dx != 0) {
		slope = (double) dy / dx;
	}

	boolean cond1 = (x1 <= x0) && (x0 < x2); // is it in the range?
	boolean cond2 = (x2 <= x0) && (x0 < x1); // is it in the reverse
												// range?
	boolean above = (y0 < slope * (x0 - x1) + y1); // point slope y - y1

	if ((cond1 || cond2) && above) {
		crossings++;
	}
}
return (crossings % 2 != 0); // even or odd
}

public void surfaceChanged(SurfaceHolder holder, int format, int width,
	int height) {
w=width;
h=height;

Terrain(0);
}

public void surfaceCreated(SurfaceHolder holder) {
main = new Thread(this);
if (main != null)
	main.start();

}

public void surfaceDestroyed(SurfaceHolder holder) {

}

@Override
public boolean onTouch(View v, MotionEvent event) {

return true;
}

public void reset()
{	
gameover = false;

Terrain(0);

firstObs = false;

x = width /2;
y = 0;
t = 0;

}

public void restart()
{	
gameover = false;

firstObs = false;

x = width /2;
y = 0;
t = 0;

currentLifes = 3;
currentLevel = 1;

initImages();

Terrain(0);

}

@Override
public void onAccuracyChanged(Sensor arg0, int arg1) {
// TODO Auto-generated method stub

}

@Override
public void onSensorChanged(SensorEvent event) {

final float noise = (float) 1.0;


if(Math.abs(event.values[1])>Math.abs(noise))
{
	float xp = event.values[1];
	
	x=x+xp*10;

	Log.d("Movement",String.valueOf(xp));
	
	side.start();
	
	if(event.values[1]>0)
		thruster = true;
	else
		thrusterR = true;

}

}

public void pause()
{
mSensorManager.unregisterListener(this);
}

public void resume()
{
mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
}

public void close()
{
	back=true;
}



}
