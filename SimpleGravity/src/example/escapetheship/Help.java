package example.escapetheship;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Help extends Activity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main4);
		TextView t = (TextView) findViewById(R.id.help);
		t.setText("The purpose of the game is to escape from an alien planet \n located in the Andromeda Galaxy." +
				"\n Move the screen to control the spaceship.");
	}
	
}


